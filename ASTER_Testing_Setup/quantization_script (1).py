
from __future__ import absolute_import
import sys
sys.path.append('./')
import numpy as np
from PIL import Image
import torch
from torch import nn
# from torch.backends import cudnn
from torchvision import transforms
from config import get_args
from models.model_builder import ModelBuilder
import string
import time
import os
import csv

def to_numpy(tensor):
  if torch.is_tensor(tensor):
    return tensor.cpu().numpy()
  elif type(tensor).__module__ != 'numpy':
    raise ValueError("Cannot convert {} to numpy array"
                     .format(type(tensor)))
  return tensor


def get_vocabulary(voc_type, EOS='EOS', PADDING='PADDING', UNKNOWN='UNKNOWN'):
  '''
  voc_type: str: one of 'LOWERCASE', 'ALLCASES', 'ALLCASES_SYMBOLS'
  '''
  voc = None
  types = ['LOWERCASE', 'ALLCASES', 'ALLCASES_SYMBOLS']
  if voc_type == 'LOWERCASE':
    voc = list(string.digits + string.ascii_lowercase)
  elif voc_type == 'ALLCASES':
    voc = list(string.digits + string.ascii_letters)
  elif voc_type == 'ALLCASES_SYMBOLS':
    voc = list(string.printable[:-6])
  else:
    raise KeyError('voc_type must be one of "LOWERCASE", "ALLCASES", "ALLCASES_SYMBOLS"')

  # update the voc with specifical chars
  voc.append(EOS)
  voc.append(PADDING)
  voc.append(UNKNOWN)
  return voc

## param voc: the list of vocabulary
def char2id(voc):
  return dict(zip(voc, range(len(voc))))

def id2char(voc):
  return dict(zip(range(len(voc)), voc))

def labels2strs(labels, id2char, char2id):
  # labels: batch_size x len_seq
  if labels.ndimension() == 1:
    labels = labels.unsqueeze(0)
  assert labels.dim() == 2
  labels = to_numpy(labels)
  strings = []
  batch_size = labels.shape[0]

  for i in range(batch_size):
    label = labels[i]
    string = []
    for l in label:
      if l == char2id['EOS']:
        break
      else:
        string.append(id2char[l])
    string = ''.join(string)
    strings.append(string)

  return strings


def load_checkpoint(fpath):
    device = torch.device("cpu")
    checkpoint = torch.load(fpath,map_location=device)
    
    print("=> Loaded checkpoint '{}'".format(fpath))
    return checkpoint

def image_process(image_path, imgH=32, imgW=224, keep_ratio=False, min_ratio=1):
  img = Image.open(image_path).convert('RGB')
  if keep_ratio:
    w, h = img.size
    ratio = w / float(h)
    imgW = int(np.floor(ratio * imgH))
    imgW = max(imgH * min_ratio, imgW)

  img = img.resize((imgW, imgH), Image.BILINEAR)
  img = transforms.ToTensor()(img)
  img.sub_(0.5).div_(0.5)
  return img

class DataInfo(object):
  """
  Save the info about the dataset.
  This a code snippet from dataset.py
  """
  def __init__(self, voc_type):
    super(DataInfo, self).__init__()
    self.voc_type = voc_type
    assert voc_type in ['LOWERCASE', 'ALLCASES', 'ALLCASES_SYMBOLS']
    self.EOS = 'EOS'
    self.PADDING = 'PADDING'
    self.UNKNOWN = 'UNKNOWN'
    self.voc = get_vocabulary(voc_type, EOS=self.EOS, PADDING=self.PADDING, UNKNOWN=self.UNKNOWN)
    self.char2id = dict(zip(self.voc, range(len(self.voc))))
    self.id2char = dict(zip(range(len(self.voc)), self.voc))

    self.rec_num_classes = len(self.voc)



def get_str_list(output, target,dataset=None):

  assert output.dim() == 2 and target.dim() == 2
  # print("output size",output)
  end_label = dataset.char2id[dataset.EOS]
  unknown_label = dataset.char2id[dataset.UNKNOWN]

  # print("end_label, unknown_label",end_label, unknown_label)
  num_samples, max_len_labels = output.size()
  # print("num_samples max_len_labels",num_samples,max_len_labels,output.size())
  num_classes = len(dataset.char2id.keys())
  # print("char2id",dataset.char2id)
  assert num_samples == target.size(0) and max_len_labels == target.size(1)
  output = to_numpy(output)
  target = to_numpy(target)
  # print("output : ",output)
  # list of char list
  pred_list, targ_list = [], []
  for i in range(num_samples):
    pred_list_i = []
    for j in range(max_len_labels):
      if output[i, j] != end_label:
        # print(output[i, j])
        if output[i, j] != unknown_label:
          # print(output[i, j],dataset.id2char[output[i, j]])
          pred_list_i.append(dataset.id2char[output[i, j]])
      else:
        break
    pred_list.append(pred_list_i)

  for i in range(num_samples):
    targ_list_i = []
    for j in range(max_len_labels):
      if target[i, j] != end_label:
        if target[i, j] != unknown_label:
          targ_list_i.append(dataset.id2char[target[i, j]])
      else:
        break
    targ_list.append(targ_list_i)

  pred_list = [''.join(pred).lower() for pred in pred_list]
  targ_list = [''.join(targ).lower() for targ in targ_list]

  return pred_list, targ_list


def load_model(args):
  # np.random.seed(args.seed)
  # torch.manual_seed(args.seed)
  # torch.cuda.manual_seed(args.seed)
  # torch.cuda.manual_seed_all(args.seed)
  # cudnn.benchmark = True
  # torch.backends.cudnn.deterministic = True

  args.cuda = args.cuda and torch.cuda.is_available()
  if args.cuda  :  
    print('using cuda.')
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
  else:
    torch.set_default_tensor_type('torch.FloatTensor')
  
  # Create data loaders
  if args.height is None or args.width is None:
    args.height, args.width = (32, 100)
  dataset_info = DataInfo(args.voc_type)

  # Create model
  model = ModelBuilder(arch=args.arch, rec_num_classes=dataset_info.rec_num_classes,
                       sDim=args.decoder_sdim, attDim=args.attDim, max_len_labels=args.max_len,
                       eos=dataset_info.char2id[dataset_info.EOS], STN_ON=False)


  if args.resume:
    checkpoint = load_checkpoint(args.resume)
    model.load_state_dict(checkpoint['state_dict'], strict=False)
    
  if args.cuda:
    device = torch.device("cpu")
    model = model.to(device)
    model = nn.DataParallel(model)
  return model

if __name__=="__main__":
  global_args = get_args(sys.argv[1:])
  args = get_args(sys.argv[1:])
  model =  load_model(args)
  #model = torch.load("/home/jayakumar/Downloads/checkpoint.pth.tar")
   # Load the PyTorch model and checkpoint
  
  checkpoint = load_checkpoint('/home/user/Downloads/Aster_testing/checkpoint.pth.tar')
  model.load_state_dict(checkpoint['state_dict'])

  quantized_model = torch.quantization.quantize_dynamic(model, {torch.nn.Linear,torch.nn.LSTM,torch.nn.GRU} , dtype=torch.qint8)
    

  torch.save(quantized_model, '_quantized_model.pth.tar')

  dataset_info = DataInfo(args.voc_type)
  model.eval()
  image_path = "/home/user/Downloads/Aster_testing/sample/"
  f = open("model_sample.csv","w")
  csv_writer= csv.writer(f)
  csv_writer.writerow(("image","text","conf"))
  image_start = time.time() 
  for image in os.listdir(image_path):
    try:
      one_image_start = time.time()
      image_start = time.time() 
      img = image_process(image_path+image) 
      image_end = time.time()
      gpu_time = time.time()
      args.cuda = args.cuda and torch.cuda.is_available()
      # if  args.cuda:
      #   device = torch.device("cuda")
      # else:
      device = torch.device("cpu")
      gpu_end = time.time()
      torch_time = time.time()
      with torch.no_grad():
        img = img.to(device)
      torch_end = time.time()
      input_dict = {}
      input_dict['images'] = img.unsqueeze(0)
      rec_targets = torch.IntTensor(1, args.max_len).fill_(1)
      rec_targets[:,args.max_len-1] = dataset_info.char2id[dataset_info.EOS]
      input_dict['rec_targets'] = rec_targets
      input_dict['rec_lengths'] = [args.max_len]
      output_dict = model(input_dict)
      pred_rec = output_dict['output']['pred_rec']
      pred_str, _ = get_str_list(pred_rec, input_dict['rec_targets'], dataset=dataset_info)
      eos_idx = (output_dict['output']['pred_rec']== 94).nonzero()[0][1]
      csv_writer.writerow((image,pred_str[0],  output_dict['output']['pred_rec_score'].tolist()[0][:eos_idx.item()]))
      print('{1} ==> {0} '.format(pred_str[0], image))
    except:
      print("invalid image")
      
  image_end = time.time()
  print("time taken : {} secs".format((image_end-image_start)*1000))
      
