CUDA_VISIBLE_DEVICES=0 python demo.py \
  --height 32 \
  --width 224 \
  --voc_type ALLCASES_SYMBOLS \
  --arch ResNet_ASTER \
  --with_lstm \
  --max_len 100 \
  --STN_ON \
  --beam_width 5 \
  --tps_inputsize 32 64 \
  --tps_outputsize 32 100 \
  --tps_margins 0.05 0.05 \
  --stn_activation none \
  --num_control_points 20 \
  --resume /home/vishwam/mountpoint/bhanu/ASTER_Training_Setup/logs/baseline_aster_87.8/model_best.pth.tar \
  --image_path /home/vishwam/mountpoint/bhanu/AADHAR_ASTER_PYTORCH/DE_8kgold/03f3e0cba0cf9bf4a57cbc0b31552b78_5.png
  