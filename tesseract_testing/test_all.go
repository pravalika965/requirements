package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"encoding/xml"
	"math/rand"
	"time"

	//"encoding/csv"
	//"encoding/json"
	//"math/rand"
	//"time"

	//"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/otiai10/gosseract"

	//"math/rand"
	"os"
	"regexp"
	"strconv"
	"strings"
	"flag"
	//"time"
	//"reflect"
)

// fmt.Println("FOO:", os.Getenv("FOO"))
// fmt.Println("BAR:", os.Getenv("BAR"))
type Letter struct {
	Id        string `xml:"id,attr"`
	Title     string `xml:"title,attr"`
	Class     string `xml:"class,attr"`
	Character string `xml:",chardata"`
}
type LetterSpan struct {
	Id     string   `xml:"id,attr"`
	Class  string   `xml:"class,attr"`
	Chosen string   `xml:"chosen,attr"`
	Letter []Letter `xml:"span"`
}

type Word struct {
	ID          string       `xml:"id,attr"`
	Title       string       `xml:"title,attr"`
	Class       string       `xml:"class,attr"`
	Characters  string       `xml:",chardata"`
	LetterSpans []LetterSpan `xml:"span"`
}

type WordConf struct {
	Word     string `json:"word"`
	WordConf string `json:"wordConf"`
}

type Choices struct {
	Letter string `json:"Letter"`
	Conf   string `json:"Conf"`
}

type LetterObj struct {
	LetterChoosen string    `json:"Chosen"`
	LetterChoices []Choices `json:"Choices"`
}

// Line struct represents `<span class='ocr_line' />`

type Line struct {
	ID     string `xml:"id,attr"`
	Title  string `xml:"title,attr"`
	Class  string `xml:"class,attr"`
	Words  []Word `xml:"span"`
	Ycord1 int
}

// Par struct represents `<p class='ocr_par' />`

type Par struct {
	ID       string `xml:"id,attr"`
	Title    string `xml:"title,attr"`
	Class    string `xml:"class,attr"`
	Language string `xml:"lang,attr"`
	Lines    []Line `xml:"span"`
}

// Content struct represents `<div class='ocr_carea' />`
type Content struct {
	ID    string `xml:"id,attr"`
	Title string `xml:"title,attr"`
	Class string `xml:"class,attr"`
	Par   Par    `xml:"p"`
}

// Page struct represents `<div class='ocr_page' />`

type Page struct {
	ID      string  `xml:"id,attr"`
	Title   string  `xml:"title,attr"`
	Class   string  `xml:"class,attr"`
	Content Content `xml:"div"`
}

func getConf(confString string) int {
	confLevel := strings.Split(confString, ";")
	re := regexp.MustCompile(`\d{2}`)
	if re.MatchString(confLevel[1]) {
		confString := re.FindAllString(confLevel[1], -1)
		conf, err := strconv.Atoi(confString[0])
		if err != nil {
			fmt.Println(err)
		}
		return conf
	}
	return 0
}
func getLetterConfidence(title string) (conf string) {

	re := regexp.MustCompile(`\d{2,3}`)
	if re.MatchString(title) {
		confString := re.FindAllString(title, -1)
		return confString[0]
	}

	return conf
}
func main() {

	//runtime.GOMAXPROCS(runtime.NumCPU())
	os.Setenv("OMP_THREAD_LIMIT", "4")
	fmt.Println("OMP_THREAD_LIMIT:", os.Getenv("OMP_THREAD_LIMIT"))

	var aadharFrontDetails = [][]string{{"File Name", "ocr full text", "MinConf", "MaxConf", "WordConf_Json", "Total_Choices", "AllChoices_String", "Choices_Conf", "New_Choices_Conf", "Total_Choosen_Characters", "p1p2_ratios"}}
	var MaxLetters = -1

	client := gosseract.NewClient()
	defer client.Close()

	files, err := ioutil.ReadDir("./VOTER_POI_6K/")
	if err != nil {
		//log.Fatal(err)
		fmt.Println(err)
	}
	        langPtr := flag.String("lang", "tr_output", "a string")
		flag.Parse()


	// OCR for each file in the aadhar front directory by looping through each file
	for I, f := range files {

		fmt.Println("******************************************************************************************:", f.Name())
		imageFile, err := os.Open( "./VOTER_POI_6K/" + f.Name())
		if err != nil {
			fmt.Println(err)
		}
		buff := bytes.NewBuffer(nil)
		_, err2 := io.Copy(buff, imageFile)
		if err2 != nil {
			fmt.Println(err2)
		}
		_ = client.SetImageFromBytes(buff.Bytes())
		//_ = client.SetConfigFile("ocr.config")
		client.Variables = map[gosseract.SettableVariable]string{
			"lstm_choice_mode": "2",
		}

		client.Languages = []string{"eng"}
		text, _ := client.Text()
		fmt.Println("Text:", text)

		// To get the text along with confidence number for the OCR
		text2, _ := client.HOCRText()
		fmt.Println("HOCR Text:", text2)

		//unmarshal Hocr
		var lineContent Page
		var MinConf = 100
		var MaxConf = -1
		err = xml.Unmarshal([]byte(text2), &lineContent)
		if err != nil {
			fmt.Println("Unable to unmarshal HOCR Text", err)
		}

		var lineWords []WordConf

		for i, _ := range lineContent.Content.Par.Lines {
			line := lineContent.Content.Par.Lines[i]

			for i, _ := range line.Words {
				word := line.Words[i]
				var tmpWord WordConf
				tmp := strings.Split(word.Characters, "\n")
				tmpWord.Word = tmp[0]
				confidence := getConf(word.Title)
				if confidence < MinConf {
					MinConf = confidence
				}
				if confidence > MaxConf {
					MaxConf = confidence
				}
				tmpWord.WordConf = strconv.Itoa(confidence)
				lineWords = append(lineWords, tmpWord)
			}
		}

		lwBytes, err2 := json.Marshal(lineWords)
		if err2 != nil {
			fmt.Println("Unable to marshal line Words Json")
		}

		fmt.Println("json:", string(lwBytes))

		singleAadharOcr := []string{f.Name(), text, strconv.Itoa(MinConf), strconv.Itoa(MaxConf), string(lwBytes)}

		var letterDetails []string
		var allChoices, choicesConf, newChoicesConf, p1p2_ratios string
		var totalChoices, totalChoosenChars int
		newChoicesConf = "("

		fmt.Println("Words:")
		for i, _ := range lineContent.Content.Par.Lines {
			eachLine := lineContent.Content.Par.Lines[i]
			var TotalLetters int
			for j, _ := range eachLine.Words {
				eachWord := eachLine.Words[j]
				splits := strings.Split(eachWord.Characters, "\n")
				eachWord.Characters = splits[0]
				fmt.Println("Line-", i, "Word-", j, ": ", eachWord.Characters)
				for k, _ := range eachWord.LetterSpans {
					eachLetterSpan := eachWord.LetterSpans[k]
					var tmpLetter LetterObj
					var confs []int
					tmpLetter.LetterChoosen = eachLetterSpan.Chosen
					TotalLetters += 1
					totalChoosenChars += 1
					//fmt.Println("Line-",i,"Word-",j," Letter-",k,": ",eachLetterSpan.Chosen)
					for l, _ := range eachLetterSpan.Letter {
						eachLetter := eachLetterSpan.Letter[l]
						var tmpChoice Choices
						tmpChoice.Letter = eachLetter.Character
						choiceConf := getLetterConfidence(eachLetter.Title)
						tmpChoice.Conf = choiceConf
						//fmt.Println(" Choice-",l,": ",eachLetter.Character," Conf:",choiceConf)
						tmpLetter.LetterChoices = append(tmpLetter.LetterChoices, tmpChoice)

						//New requirements code
						totalChoices += 1
						allChoices = allChoices + tmpChoice.Letter
						if choiceConf == "" {
							choicesConf = choicesConf + "0" + ","
							confs = append(confs, 0)
						} else {
							choicesConf = choicesConf + choiceConf + ","
							confInt, _ := strconv.Atoi(choiceConf)
							confs = append(confs, confInt)
						}

					}
					if len(confs) == 1 {
						eachConfString := "(" + strconv.Itoa(confs[0]) + ",0,0),"
						newChoicesConf = newChoicesConf + eachConfString
						p1p2Ratio := strconv.Itoa(confs[0])
						p1p2_ratios = p1p2_ratios + p1p2Ratio + ","

					} else if len(confs) == 2 {
						eachConfString := "(" + strconv.Itoa(confs[0]) + "," + strconv.Itoa(confs[1]) + ",0),"
						newChoicesConf = newChoicesConf + eachConfString
						if confs[1] != 0 {
							p1p2Ratio := float64(confs[0]) / float64(confs[1])
							p1p2_ratios = p1p2_ratios + strconv.FormatFloat(p1p2Ratio, 'g', 6, 64) + ","
						} else {
							p1p2Ratio := strconv.Itoa(confs[0])
							p1p2_ratios = p1p2_ratios + p1p2Ratio + ","
						}

					} else if len(confs) == 3 || len(confs) > 3 {
						eachConfString := "(" + strconv.Itoa(confs[0]) + "," + strconv.Itoa(confs[1]) + "," + strconv.Itoa(confs[2]) + "),"
						newChoicesConf = newChoicesConf + eachConfString
						if confs[1] != 0 {
							p1p2Ratio := float64(confs[0]) / float64(confs[1])
							p1p2_ratios = p1p2_ratios + strconv.FormatFloat(p1p2Ratio, 'g', 6, 64) + ","
						} else {
							p1p2Ratio := strconv.Itoa(confs[0])
							p1p2_ratios = p1p2_ratios + p1p2Ratio + ","
						}
					}
					fmt.Println("Each Letter Details: ", k)
					letterBytes, err2 := json.Marshal(tmpLetter)
					if err2 != nil {
						fmt.Println("Unable to marshal line Words Json")
					}
					fmt.Println("json_here:", string(letterBytes))
					letterDetails = append(letterDetails, string(letterBytes))
				}
			}

			fmt.Println("Total Letters:", TotalLetters)
			if TotalLetters > MaxLetters {
				MaxLetters = TotalLetters
			}
		}

		singleAadharOcr = append(singleAadharOcr, strconv.Itoa(totalChoices))
		singleAadharOcr = append(singleAadharOcr, allChoices)

		choicesConf = strings.Trim(choicesConf, ",")
		singleAadharOcr = append(singleAadharOcr, choicesConf)

		newChoicesConf = strings.Trim(newChoicesConf, ",")
		newChoicesConf = newChoicesConf + ")"
		singleAadharOcr = append(singleAadharOcr, newChoicesConf)

		singleAadharOcr = append(singleAadharOcr, strconv.Itoa(totalChoosenChars))

		p1p2_ratios = strings.Trim(p1p2_ratios, ",")
		singleAadharOcr = append(singleAadharOcr, p1p2_ratios)

		fmt.Println("New Details - ", I, totalChoices, allChoices, choicesConf)
		fmt.Println("New Choices - ", newChoicesConf)
		fmt.Println("Sukku Requirement:", totalChoosenChars, p1p2_ratios)

		for i, _ := range letterDetails {
			singleAadharOcr = append(singleAadharOcr, letterDetails[i])
		}
		aadharFrontDetails = append(aadharFrontDetails, singleAadharOcr)
	}

	for N := 1; N <= MaxLetters; N++ {
		columnHeader := "Letter_" + strconv.Itoa(N)
		aadharFrontDetails[0] = append(aadharFrontDetails[0], columnHeader)
	}

	//Write the complete csv array/rows into csv file
	rand.Seed(time.Now().UnixNano())
	// ran := strconv.Itoa(rand.Int())
	
       
	reportname := *langPtr + ".csv"
	file, err := os.Create("csv_reports/" + reportname)
	if err != nil {
		fmt.Println(err)
	}

	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()
	for index, _ := range aadharFrontDetails {
		err := writer.Write(aadharFrontDetails[index])
		if err != nil {
			fmt.Println(err)
		}
	}
}
